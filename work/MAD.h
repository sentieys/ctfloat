#include <cstdlib>
#include <ctime>
#include <iostream>
#include <math.h>

#include "ctfloat.h"
using namespace std;

#ifndef EXP
#define EXP 8
#endif
#ifndef MAN
#define MAN 24
#endif

typedef ct_float<EXP, MAN, CT_RD> myfloat;

void MAD_double(double x, double h, double y, double *z);
void MAD_float(float x, float h, float y, float *z);
void MAD_ctfloat(ct_float<EXP, MAN, CT_RD> x, ct_float<EXP, MAN, CT_RD> h, ct_float<EXP, MAN, CT_RD> y, ct_float<EXP, MAN, CT_RD> *z);


