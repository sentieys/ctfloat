#include "FIR.h"


void fir_float(float x[N_DATA], float y[N_DATA - N_FIR]){
//#pragma HLS ARRAY_PARTITION variable=x complete dim=1

	fir_float_label0:for (int i = 0; i < N_DATA - N_FIR; i++) {
#pragma HLS PIPELINE
		y[i] = x[i] * h[N_FIR - 1];
		for (int j = 1; j < N_FIR; j++) {
#pragma HLS UNROLL
			y[i] += x[i + j] * h[N_FIR - j - 1];
		}
	}


}
