#include "FIR.h"


int main(void) {

	double x[N_DATA], y[N_DATA - N_FIR];
	ct_float<EXP, MAN, CT_RD> x_ctf[N_DATA], y_ctf[N_DATA - N_FIR];

	srand(time(NULL));

	// Random inputs between -1 and 1 
	for (int i = 0; i < N_DATA; i++) {
		x[i] = ((double) rand() / (double) RAND_MAX);
		x[i] = (rand() % 2) ? x[i] : -x[i];
		x_ctf[i] = ct_float<EXP, MAN, CT_RD>(x[i]);
	}


	// Reference filter
	for (int i = 0; i < N_DATA - N_FIR; i++) {
	  y[i] = x[i] * h_d[N_FIR - 1];
	  for (int j = 1; j < N_FIR; j++) {
            y[i] += x[i + j] * h_d[N_FIR - j - 1];
	  }
	}
 
	fir_ctfloat(x_ctf,y_ctf);
	

	cout << "FIR order " << (int) N_FIR << " (converti en ct_float<" << ct_float<8, 24, CT_RD>::exponent_w << "," << ct_float<8, 24, CT_RD>::mantissa_w + 1 << ">):" << endl;
	for (int i = 0; i < N_FIR; i++) {
		cout << h_ctf[i] << "\t" << h[i] << endl;
	}
	cout << endl;

	cout << "Results (ctfloat, double, error):" << endl;
	for (int i = 0; i < N_DATA-N_FIR; i++) {
	  cout << y_ctf[i] << "\t" << y[i] << "\t" << y[i]-(double)y_ctf[i] << endl;
	}




	return EXIT_SUCCESS;
}
