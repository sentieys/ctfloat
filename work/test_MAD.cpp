#include "MAD.h"

int main(void) {

	myfloat input1[5];
	myfloat input2[5];
	myfloat result = 0.0;

	float input1_float[5], input2_float[5];
	float result_float = 0.0;

	double input1_double[5] = {0.15489465, 1.48465498, -2.86584651, -0.484654321, 7.1854351432};
	double input2_double[5] = {-4.4584625136, 5.19843213654, -0.0549843251, 1.486462516514, 2.1894651465};
	double result_double = 0.0;


	for (int i = 0; i < 5; i++) {
		input1[i] = input1_double[i];
		input1_float[i] = input1_double[i];
		input2[i] = input2_double[i];
		input2_float[i] = input2_double[i];
	}

// Opération de multiplication accumulation

	for (int i = 0; i < 5; i++) {
		MAD_ctfloat(input1[i], input2[i], result, &result);
		cout << "result (ct_float) = " << result << endl;

		MAD_float(input1_float[i], input2_float[i], result_float, &result_float);
		cout << "result (float) = " << result_float << endl ;

		MAD_double(input1_double[i], input2_double[i], result_double, &result_double);
		cout << "result (double) = " << result_double << endl << endl;
	}

	cout << "Error vs. double (ct_float): " << result_double-(double)result << endl;
	cout << "Error vs. double (float): " << result_double-(double)result_float << endl;

	return 0;
}
