#include "MAD.h"

// z = x*h+y = MAD(x,h,y)
void MAD_double(double x, double h, double y, double *z){
	*z = x*h+y;
};

void MAD_float(float x, float h, float y, float *z){
	*z = x*h+y;
};

void MAD_ctfloat(ct_float<EXP, MAN, CT_RD> x, ct_float<EXP, MAN, CT_RD> h, ct_float<EXP, MAN, CT_RD> y, ct_float<EXP, MAN, CT_RD> *z){
	ct_float<EXP, MAN, CT_RD> t;
	t = x*h+y;
	*z = t;
};



