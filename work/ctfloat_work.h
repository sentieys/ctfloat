/* CT_FLOAT 1.0 by Benjamin Barrois - started Wed 15 Feb 2017
 CatapultC synthesizable custom floating point library for application optimization
 Using ac_int type and freely inspired from ac_float library
 Made for freely customizing arithmetic operators, mainly for approximate computing experiments
 */

#ifndef _CT_FLOAT_H_
#define _CT_FLOAT_H_

#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>
#include <bitset>

//Define HLS tools
#define CATAPULT 1
#define VIVADO 0
#define VIVADO_OR_CATAPULT VIVADO

#if VIVADO_OR_CATAPULT == 0
#include "ap_int.h"
#elif VIVADO_OR_CATAPULT == 1
#include "ac_int.h"
#endif

// Equivalence between catapult === vivado
// x.slc<n>(k) === x(k+n-1,k)
// x.set_slc(k,y) === x(k+y.length()-1,k) = y


// Shortcuts for templates
#define CT_FLOAT_T(v) unsigned int EXP_W##v, unsigned int MANT_W##v, ct_q_mode Q_MODE##v
#define CT_FLOAT_T_NOQ(v) unsigned int EXP_W##v, unsigned int MANT_W##v
#define CT_FLOAT_DEC(v) ct_float<EXP_W##v, MANT_W##v, Q_MODE##v>

// Functions
#define CT_MAX(a,b) ((a) > (b) ? (a) : (b))
#define CT_MIN(a,b) ((a) < (b) ? (a) : (b))
#define CT_ABS(a) ((a) < 0 ? (-a) : (a))

enum ct_q_mode {
	CT_RD, CT_RU, CT_RND, CT_RNU
};


// modified OS
template <int x>
 struct static_log2 { enum { value = 1 + static_log2<x/2>::value }; };

template <> struct static_log2<1> { enum { value = 1 }; };


template<CT_FLOAT_T()> class ct_float {
public:
	// Types definitions

#if VIVADO_OR_CATAPULT == 0
	typedef ap_uint<1> sign_t;
	typedef ap_int<EXP_W> exp_t;
	typedef ap_uint<MANT_W - 1> mant_t;
#elif VIVADO_OR_CATAPULT == 1
	typedef ac_int<1, false> sign_t;
	typedef ac_int<EXP_W, true> exp_t;
	typedef ac_int<MANT_W - 1, false> mant_t;
#endif

	// Class attributes
	sign_t s;
	exp_t e;
	mant_t m;

	// Creating friend class ct_float for overloading
	template<CT_FLOAT_T(2)> friend class ct_float;
	template<CT_FLOAT_T(3)> friend class ct_float;

	// Constructors
			ct_float() {}
			ct_float(const double in) {
				this->operator =(in);
			}
			ct_float(const float in) {
				this->operator =(in);
			}

			//Destructor
			~ct_float() {}

			// Accessers
			sign_t get_sign() {return s;};
			sign_t get_exponent() {return e;};
			sign_t get_mantissa() {return m;};

			// Modifiers
			void set_sign(const sign_t sign) {s = sign;}
			void set_exponent(const exp_t exp) {e = exp;}
			void set_mantissa(const mant_t man) {m = man;}

			// Static values
			static const unsigned int width = EXP_W + MANT_W;
			static const unsigned int sign_w = 1;
			static const unsigned int exponent_w = EXP_W;
			static const unsigned int mantissa_w = MANT_W-1;
            static const unsigned int two_pow_mantissa_w = (((int) 1) << (MANT_W-1));
			static const long min_exponent_val = -(((long) 1) << (EXP_W-1));
			static const long max_exponent_val = (((long) 1) << (EXP_W-1))-1;
			static const long max_mantissa_val = (((long) 1) << (mantissa_w))-1;
			static const ct_q_mode q_mode_val = Q_MODE;

			static double get_max_abs_val_double() {
				ct_float temp;
				double r;
				temp.s = 0;
				temp.e = max_exponent_val;
				temp.m = max_mantissa_val;
				r = (double) temp;
				return r;
			}
			static double get_min_abs_val_double() {
				ct_float temp;
				double r;
				temp.s = 0;
				temp.e = min_exponent_val;
				temp.m = 0;
				r = (double) temp;
				return r;
			}

			bool is_zero() {
				return ((!!s) && (e == exp_t(min_exponent_val)) && !m);
			}

			bool is_zero(sign_t s_in, exp_t e_in, mant_t m_in) {
				return ((!!s_in) && (e_in == exp_t(min_exponent_val)) && !m_in);
			}

			void set_to_zero() {
				s = sign_t(1);
				e = min_exponent_val;
				m = mant_t(0);
			}
			void set_to_zero(sign_t *s_in, exp_t *e_in, mant_t *m_in) {
				*s_in = sign_t(1);
				*e_in = min_exponent_val;
				*m_in = mant_t(0);
			}

			void sat_max_pos() {
				s = sign_t(0);
				e = max_exponent_val;
				m = max_mantissa_val;
			}

			void sat_max_pos(sign_t *s_in, exp_t *e_in, mant_t *m_in) {
				*s_in = sign_t(0);
				*e_in = max_exponent_val;
				*m_in = max_mantissa_val;
			}

			void sat_min_pos() {
				s = sign_t(0);
				e = min_exponent_val;
				m = mant_t(0);
			}

			void sat_min_pos(sign_t *s_in, exp_t *e_in, mant_t *m_in) {
				*s_in = sign_t(0);
				*e_in = min_exponent_val;
				*m_in = mant_t(0);
			}

			void sat_max_neg() {
				s = sign_t(1);
				e = min_exponent_val;
				m = mant_t(1);
			}

			void sat_max_neg(sign_t *s_in, exp_t *e_in, mant_t *m_in) {
				*s_in = sign_t(1);
				*e_in = min_exponent_val;
				*m_in = mant_t(1);
			}

			void sat_min_neg() {
				s = sign_t(1);
				e = max_exponent_val;
				m = max_mantissa_val;
			}

			void sat_min_neg(sign_t *s_in, exp_t *e_in, mant_t *m_in) {
				*s_in = sign_t(1);
				*e_in = max_exponent_val;
				*m_in = max_mantissa_val;
			}

			template<CT_FLOAT_T_NOQ(2)> struct arith_rt {
				enum {
					add_sub_exp_w = CT_MAX(EXP_W, EXP_W2),
					add_sub_mant_w = CT_MAX(MANT_W, MANT_W2),
					mul_exp_w = CT_MAX(EXP_W, EXP_W2),
					mul_mant_w = CT_MAX(MANT_W, MANT_W2)
				};
				typedef ct_float<add_sub_exp_w, add_sub_mant_w,Q_MODE> add_sub_t;
				typedef ct_float<mul_exp_w, mul_mant_w,Q_MODE> mul_t;
			};

			// Arithmetic functions
			template<CT_FLOAT_T(2)> void add_func(const CT_FLOAT_DEC(2) &op2) {

				typedef ct_float out_t;

#if VIVADO_OR_CATAPULT == 0
				typedef ap_uint<out_t::mantissa_w+1> mants1_t;
				typedef ap_int<out_t::mantissa_w+3> mants2_t;
				typedef ap_uint<out_t::mantissa_w> mant_out_t;
				typedef ap_int<out_t::exponent_w> exps_t;
				typedef ap_int<out_t::exponent_w> exp_out_t;
				typedef ap_int<out_t::exponent_w+1> shift_t;
				typedef ap_uint<out_t::exponent_w+2> shiftu_t;
#elif VIVADO_OR_CATAPULT == 1
				typedef ac_int<out_t::mantissa_w+1,false> mants1_t;
				typedef ac_int<out_t::mantissa_w+3,true> mants2_t;
				typedef ac_int<out_t::mantissa_w,false> mant_out_t;
				typedef ac_int<out_t::exponent_w,true> exps_t;
				typedef ac_int<out_t::exponent_w,true> exp_out_t;
				typedef ac_int<out_t::exponent_w+1,true> shift_t;
				typedef ac_int<out_t::exponent_w+2,false> shiftu_t;
#endif

				typedef bool bit_t;

				// Declarations
				sign_t s1, s2;
				exps_t e1, e2;
				shift_t e_dif;
				shiftu_t e_dif_unsigned;
				mants1_t m1,m2,m_inf,sticky_bits;
				mants2_t m1_temp, m2_temp, m_out_temp;

				const mants1_t masq = ~((~mants1_t(0)) << (out_t::mantissa_w));
				bit_t r_bit = 0, s_bit = 0;// Round bit and sticky bit
				bit_t e1_lt_e2;
				bit_t op1_iszero, op2_iszero;

				sign_t s_out;
				exp_out_t e_out;
				mant_out_t m_out;

				out_t op2_temp;
				op2_temp = op2;// Cast for having both inputs same size

				// DEBUG
				/*std::cout << "Cast input: " << op2 << "\n";
				 std::cout << "s: " << op2.s << "\te: " << op2.e << "\tm: " << op2.m << "\n";
				 std::cout << "Cast output: " << op2_temp << "\n";
				 std::cout << "s: " << op2_temp.s << "\te: " << op2_temp.e << "\tm: " << op2_temp.m << "\n\n";*/

				// Assignments
				s1 = s;
				s2 = op2_temp.s;
				e1 = e;
				e2 = op2_temp.e;
				m1 = m;
				m2 = op2_temp.m;

				// Checking if one input is zero and adding hidden 1 if needed
				op1_iszero = operator !();
				op2_iszero = !op2_temp;


				// Computing mantissa shift value
				bool one_input_is_zero = op1_iszero || op2_iszero;
				if (op1_iszero) {
					e1_lt_e2 = true;
				} else if(op2_iszero) {
					e1_lt_e2 = false;
				} else {
				e_dif = e1 - e2;
				e1_lt_e2 = e_dif < 0;// Value for shifting
				e_dif_unsigned = e1_lt_e2 ? shiftu_t(-e_dif) : shiftu_t(e_dif);
				// Adding hidden 1
				m1[out_t::mantissa_w] = 1;
				m2[out_t::mantissa_w] = 1;
				m_inf = e1_lt_e2 ? m1 : m2;
				}

				if((!one_input_is_zero) && (e_dif_unsigned <= out_t::mantissa_w)) { // CLOSE PATH

					// Extracting sticky and round bits
					sticky_bits = m_inf;
					sticky_bits &= ~((~mants1_t(0)) << e_dif_unsigned);
					s_bit = !!sticky_bits;
					r_bit = (bit_t) m_inf[e_dif_unsigned];

					// Shifting
					m_inf >>= e_dif_unsigned;

					// Putting back variables in place
					m1 = e1_lt_e2 ? m_inf : m1;
					m2 = e1_lt_e2 ? m2 : m_inf;
					// Setting up temporary output exponent
					e_out = e1_lt_e2 ? e2 : e1;

					// Addition depending on sign
					bit_t diff_sign = s1^s2;
					m1_temp = m1;
					m2_temp = diff_sign ? (mants2_t) -m2: (mants2_t) m2;

					// #### YOU CAN USE INTEGER APPROXIMATE ADDER HERE (if you'd like to)! ####
					m1_temp += m2_temp;
					s_out = m1_temp < 0;
					m_out_temp = s_out ? (mants2_t)-m1_temp : (mants2_t) m1_temp;
					s_out = s1 ? (sign_t) ~s_out : s_out;

					// ROUNDING
					this->round(m_out_temp,r_bit,s_bit);

					// Checking if m_out_temp is 0 and acting
					if(!m_out_temp) { // if zero
						set_to_zero(&s_out, &e_out, &m_out);
					} else {
						// Normalization
						if (m_out_temp[out_t::mantissa_w+1] == 1) {
							if(e_out < exp_out_t(out_t::max_exponent_val)) {
								e_out += 1;
								m_out_temp >>= 1;
								m_out = m_out_temp;
							} else {
								// Highest possible value
								m_out = ~mant_out_t(0);
							}
						} else if (m_out_temp[out_t::mantissa_w] == 0) {
							shift_t slack_exponent = shift_t(e_out) - shift_t(out_t::min_exponent_val);
							m_out = m_out_temp;
							// Counting leading zeros
#if VIVADO_OR_CATAPULT == 0	
							//ap_uint<ceil(log2(mant_out.length()) + 1)> ls_m_out = m_out.taille_de_la_chaine_de_zeros_en_partant_du_MSB()+1
							ap_uint<static_log2<out_t::mantissa_w>::value> ls_m_out = 1;
							for (int i=out_t::mantissa_w-1; i>=0; i--){
								#pragma HLS UNROLL
								if (m_out[i]){
							       break;
							    }
								ls_m_out++;
							}
							//std::cout << "m_out=" << m_out << " " << std::bitset<out_t::mantissa_w>((int) m_out) << "\tout_t::mantissa_w=" << out_t::mantissa_w << "\tls_m_out=" << ls_m_out << "\n";
#elif VIVADO_OR_CATAPULT == 1
							typename mant_out_t::rt_unary::leading_sign ls_m_out = m_out.leading_sign()+1;
							//std::cout << "m_out=" << m_out << " " << std::bitset<out_t::mantissa_w>((int) m_out) << "\tout_t::mantissa_w=" << out_t::mantissa_w << "\tls_m_out=" << ls_m_out << "\n";
#endif	
							// Shifting
							if (ls_m_out <= slack_exponent) {
#if VIVADO_OR_CATAPULT == 0	
						                e_out -= exp_out_t(int(ls_m_out));
						                m_out <<= exp_out_t(int(ls_m_out));
#elif VIVADO_OR_CATAPULT == 1
						                e_out -= ls_m_out;
						                m_out <<= ls_m_out;
#endif	
							} else {
								// Most little possible value
								e_out = exp_out_t(out_t::min_exponent_val);
								if (!s_out) { // positive
									m_out = mant_t(0);
								} else {
									m_out = mant_t(1);
								}

							}

						} else {
							m_out = m_out_temp;
						}
					}
				} else { // FAR PATH
					// NOTE: if e_dif == mantissa_w, rounding could be performed. It is not tested and performed here. There can be a slight accuracy loss on LSB, but it is worth the economy (I think).
					m_out = e1_lt_e2 ? m2 : m1;
					e_out = e1_lt_e2 ? e2 : e1;
					s_out = e1_lt_e2 ? s2 : s1;
				}

				s = s_out;
				e = e_out;
				m = m_out;
			}
			template<CT_FLOAT_T(2)> void mul_func(const CT_FLOAT_DEC(2) &op2) {

#if VIVADO_OR_CATAPULT == 0
				typedef ap_uint<mantissa_w+1> mants1_t;
				typedef ap_uint<mantissa_w+2> mants2_t;
				typedef ap_int<exponent_w+2> exps2_t;
				typedef ap_uint<2*(mantissa_w+1)> mult_out_t;
#elif VIVADO_OR_CATAPULT == 1
				typedef ac_int<mantissa_w+1,false> mants1_t;
				typedef ac_int<mantissa_w+2,false> mants2_t;
				typedef ac_int<exponent_w+2,true> exps2_t;
				typedef ac_int<2*(mantissa_w+1),false> mult_out_t;
#endif

				typedef bool bit_t;

				bit_t s_bit, r_bit;
				mants1_t LSB_mult;
				mants2_t MSB_mult;
				exps2_t exp_add;
				ct_float op2_temp = op2;
				bool op1_iszero, op2_iszero;
				mult_out_t int_mult_out;
				sign_t s1 = s, s2 = op2_temp.s;
				exp_t e1 = e, e2 = op2_temp.e;
				mants1_t m1 = mants1_t(m), m2 = mants1_t(op2_temp.m);

				sign_t s_out;
				exp_t e_out;
				mant_t m_out;

				// Restoring hidden 1;
				op1_iszero = operator !();
				op2_iszero = !op2_temp;
				if (op1_iszero || op2_iszero) {
					set_to_zero(&s_out, &e_out, &m_out);
				} else {

					// Sign management
					s_out = s1^s2;

					m1[mantissa_w] = 1;
					m2[mantissa_w] = 1;

					// Adding exponents
//					e2 += mantissa_w+1;
//					e2 += 1;
					exp_add = exps2_t(e1) + exps2_t(e2);

					// Performing mantissa multiplication
					int_mult_out = m1 * m2;
					// DEBUG
//					std::cout << m1 << " * " << m2 << " = " << int_mult_out << "\n";
#if VIVADO_OR_CATAPULT == 0	
					LSB_mult = int_mult_out(mantissa_w,0);
					MSB_mult(mantissa_w,0) = int_mult_out(2*mantissa_w+1,mantissa_w+1);
					MSB_mult[mantissa_w+1] = 0;
					// DEBUG
//					std::cout << "res_mult1: " << int_mult_out << "\n";
//					std::cout << "res_mult2: " << mult_out_t(LSB_mult) + mult_out_t(MSB_mult) * mult_out_t(256) << "\n\n";

					s_bit = !!LSB_mult(mantissa_w-1,0); 
					r_bit = LSB_mult[mantissa_w];
#elif VIVADO_OR_CATAPULT == 1
					LSB_mult = int_mult_out.template slc<mantissa_w+1>(0);
					MSB_mult.set_slc(0,int_mult_out.template slc<mantissa_w+1>(mantissa_w+1));
					MSB_mult[mantissa_w+1] = 0;
					// DEBUG
//					std::cout << "res_mult1: " << int_mult_out << "\n";
//					std::cout << "res_mult2: " << mult_out_t(LSB_mult) + mult_out_t(MSB_mult) * mult_out_t(256) << "\n\n";

					s_bit = !!LSB_mult.template slc<mantissa_w>(0);
					r_bit = LSB_mult[mantissa_w];
#endif	

					// Rounding
					this->round(MSB_mult,r_bit,s_bit);

					// Mantissa normalization
					if(MSB_mult[mantissa_w] == 0) {
						MSB_mult <<= 1;
					} else {
						exp_add += 1;
					}

					// Managing possible saturation
					if(exp_add > exps2_t(max_exponent_val)) {
						// Saturation to highest possible value
						e_out = max_exponent_val;
						m_out = max_mantissa_val;
					} else if (exp_add < exps2_t(min_exponent_val)) {
						// Saturation to most little possible value
						e_out = min_exponent_val;
						if(!s_out) {
							m_out = 0;
						}
						else {
							m_out = 1;
						}
					}
					else {
#if VIVADO_OR_CATAPULT == 0	
					        e_out = exp_add(exponent_w-1,0);   
					        m_out = MSB_mult(mantissa_w-1,0);  
#elif VIVADO_OR_CATAPULT == 1
						e_out = exp_add.template slc<exponent_w>(0);
						m_out = MSB_mult.template slc<mantissa_w>(0);
#endif	
					}

				}
				s = s_out;
				e = e_out;
				m = m_out;

			}

	// Rounding function
#if VIVADO_OR_CATAPULT == 0	
	template<int input_width> void round(ap_uint<input_width> input_int, bool r_bit, bool s_bit) {	
		if (Q_MODE == CT_RD) {
			// Do nothing
		} else if( Q_MODE == CT_RU ) {
			input_int = (r_bit || s_bit) ? input_int += 1 : input_int;
		} else if( Q_MODE == CT_RNU ) {
			input_int = r_bit ? input_int += 1 : input_int;
		} else { // CT_RND
			input_int = (r_bit && s_bit) ? input_int += 1 : input_int;
		}
	}
	template<int input_width> void round(ap_int<input_width> input_int, bool r_bit, bool s_bit) {	
		if (Q_MODE == CT_RD) {
			// Do nothing
		} else if( Q_MODE == CT_RU ) {
			input_int = (r_bit || s_bit) ? input_int += 1 : input_int;
		} else if( Q_MODE == CT_RNU ) {
			input_int = r_bit ? input_int += 1 : input_int;
		} else { // CT_RND
			input_int = (r_bit && s_bit) ? input_int += 1 : input_int;
		}
	}
#elif VIVADO_OR_CATAPULT == 1
	template<int input_width, bool input_signed> void round(ac_int<input_width,input_signed> input_int, bool r_bit, bool s_bit) {	
		if (Q_MODE == CT_RD) {
			// Do nothing
		} else if( Q_MODE == CT_RU ) {
			input_int = (r_bit || s_bit) ? input_int += 1 : input_int;
		} else if( Q_MODE == CT_RNU ) {
			input_int = r_bit ? input_int += 1 : input_int;
		} else { // CT_RND
			input_int = (r_bit && s_bit) ? input_int += 1 : input_int;
		}
	}
#endif	

	// Operators overload

	// ASSIGNMENTS
	template<CT_FLOAT_T(2)> void operator =(const CT_FLOAT_DEC(2) &op2) {
#if VIVADO_OR_CATAPULT == 0
		typedef ap_int<CT_MAX(EXP_W,EXP_W2)+1> e_temp_t;
#elif VIVADO_OR_CATAPULT == 1
		typedef ac_int<CT_MAX(EXP_W,EXP_W2)+1, true> e_temp_t;
#endif		

		e_temp_t e_temp;
		bool exp_saturation = false;
		if(!op2) {
			set_to_zero();
		} else {
			s = op2.s;
			// copying exponent
			if(exponent_w == op2.e) {
				e = op2.e;
			} else if (exponent_w < op2.e) {
				if (op2.e > max_exponent_val) {
					e = max_exponent_val;
					m = max_mantissa_val;
					exp_saturation = true;
				} else if (op2.e < min_exponent_val) {
					e = min_exponent_val;
					if(!op2.s) m = mant_t(0);
					else m = mant_t(1);
					exp_saturation = true;
				} else {
					e = op2.e;
				}
			} else {
#if VIVADO_OR_CATAPULT == 0	
			        e = op2.e(exponent_w-1,0);
#elif VIVADO_OR_CATAPULT == 1
				e = op2.e.template slc<exponent_w>(0);
#endif	
			}
			// mantissa
			if (!exp_saturation) {
				if(mantissa_w == op2.mantissa_w) m = op2.m;
				else if(mantissa_w < op2.mantissa_w) {
#if VIVADO_OR_CATAPULT == 0	
				        m = op2.m(op2.mantissa_w-1,op2.mantissa_w-mantissa_w);
#elif VIVADO_OR_CATAPULT == 1
					m = op2.m.template slc<mantissa_w>(op2.mantissa_w - mantissa_w);
#endif	
					e_temp = e_temp_t(e) + e_temp_t(op2.mantissa_w - mantissa_w);
					if (e_temp > max_exponent_val) {
						e = max_exponent_val;
						m = max_mantissa_val;
					} else if (e_temp < min_exponent_val) {
						e = min_exponent_val;
						if(!op2.s) m = mant_t(0);
						else m = mant_t(1);
					} else {
						e += (op2.mantissa_w - mantissa_w);
					}
				}
				else {
					m = mant_t(op2.m) << (mantissa_w-op2.mantissa_w);
					e_temp = e_temp_t(e) - e_temp_t(mantissa_w - op2.mantissa_w);
					if (e_temp > max_exponent_val) {
						e = max_exponent_val;
						m = max_mantissa_val;
					} else if (e_temp < min_exponent_val) {
						e = min_exponent_val;
						if(!op2.s) m = mant_t(0);
						else m = mant_t(1);
					} else {
						e -= (mantissa_w-op2.mantissa_w);
					}

				}
			}
		}

		//DEBUG
		/*std::cout << "Cast input: " << op2 << "\n";
		 std::cout << "Cast output: " << op2 << "\n\n";*/
	}
	void operator =(const double op2) {
#if VIVADO_OR_CATAPULT == 0
		typedef ap_uint<mantissa_w+1> mant_temp_t;
#elif VIVADO_OR_CATAPULT == 1
		typedef ac_int<mantissa_w+1,false> mant_temp_t;
#endif		

		double op2_temp;
		mant_temp_t mant_temp;
		ct_float r;
		int shift_count = 0;
		if(!op2) {
			set_to_zero();
		} else if (op2 == -get_min_abs_val_double()) {
			sat_max_neg();
		} else {
			s = op2 < 0 ? 1 : 0;
			op2_temp = op2 < 0 ? -op2 : op2;

			while (op2_temp < 1.0) {
				op2_temp = op2_temp*2.0;
				shift_count --;
			}

			while (op2_temp >= 2.0) {
				op2_temp = op2_temp/2.0;
				shift_count ++;
			}
			e = shift_count;

			op2_temp = op2_temp*pow(2.0,(double) mantissa_w);
			mant_temp = mant_temp_t((long) op2_temp);
#if VIVADO_OR_CATAPULT == 0	
			m = mant_temp(mantissa_w-1,0);
#elif VIVADO_OR_CATAPULT == 1
			m = mant_temp.template slc<mantissa_w>(0);
#endif	
		}
	}
	void operator =(const float op2) {
		double op2_temp = (double) op2;
#if VIVADO_OR_CATAPULT == 0
		typedef ap_uint<mantissa_w+1> mant_temp_t;
#elif VIVADO_OR_CATAPULT == 1
		typedef ac_int<mantissa_w+1,false> mant_temp_t;
#endif		
		mant_temp_t mant_temp;
		mant_temp_t mask = ~(mant_temp_t(1 << (mantissa_w+1)));
		ct_float r;
		int shift_count = 0;
		if(!op2_temp) {
			set_to_zero();
		} else if (op2_temp == -get_min_abs_val_double()) {
			sat_max_neg();
		} else {
			s = op2_temp < 0 ? 1 : 0;
			op2_temp = op2_temp < 0 ? -op2_temp : op2_temp;

			while (op2_temp < 1) {
				//op2_temp <<= 1;      // modified OS
                op2_temp = op2_temp * 2;
				shift_count --;
			}

			while (op2_temp >= 2) {
				//op2_temp >>= 1;      // modified OS
                op2_temp = op2_temp / 2;
				shift_count ++;
			}
			e = shift_count;

			//op2_temp <<= mantissa_w;      // modified OS
            op2_temp = op2_temp * two_pow_mantissa_w;
			mant_temp = mant_temp_t((long) op2_temp) & mask;
#if VIVADO_OR_CATAPULT == 0	
			m = mant_temp(mantissa_w-1,0);
#elif VIVADO_OR_CATAPULT == 1
			m = mant_temp.template slc<mantissa_w>(0);
#endif	
		}
	}
	// RELATIONAL
	template<CT_FLOAT_T(2)> bool operator ==(const CT_FLOAT_DEC(2) &op2) const {
		bool r;
		if ( (exponent_w == op2.exponent_w) && (mantissa_w == op2.mantissa_w) ) {
			r = ((s == op2.s) && (e == op2.e) && (m == op2.m));
		} else {
			ct_float ct_float_temp;
			ct_float_temp.operator=(op2);
			r = ((s == ct_float_temp.s) && (e == ct_float_temp.e) && (m == ct_float_temp.m));
			// DEBUG
			//std::cout << s << "\t" << e << "\t" << m << "\t" << ct_float_temp.s << "\t" << ct_float_temp.e << "\t" << ct_float_temp.m << "\n";
		}
		return r;
	}
	template <CT_FLOAT_T(2)> bool operator <(const CT_FLOAT_DEC(2) &op2) const {
		bool r = false;
		bool op1_iszero, op2_iszero;
		op1_iszero = operator !();
		op2_iszero = !op2;
		if(s && !op2.s) {
			r = true;
		} else if (!s && !op2.s) {
			if(op1_iszero) {
				if (!op2.s) r = true;
			} else if(op2_iszero) {
				if (s) r = true;
			} else if (e < op2.e) r = true;
			else if (e == op2.e) {
				if (m < op2.m) r = true;
			}
		} else if (s && op2.s) {
			if (e > op2.e) r = true;
			else if (e == op2.e) {
				if (m > op2.m) r = true;
			}
		}
		return r;
	}
	template <CT_FLOAT_T(2)> bool operator >(const CT_FLOAT_DEC(2) &op2) const {
		return (op2 < (*this));
	}
	template <CT_FLOAT_T(2)> bool operator <=(const CT_FLOAT_DEC(2) &op2) const {
		bool r = operator ==(op2);
		if(!r) r = operator <(op2);
		return r;
	}
	template <CT_FLOAT_T(2)> bool operator >=(const CT_FLOAT_DEC(2) &op2) const {
		bool r = operator ==(op2);
		if(!r) r = operator >(op2);
		return r;
	}
	// BINARY OPERATORS
	bool operator !() const {
		// ct_float is zero if saturation to -0.0000000000000000000000000.......00001 (s = 1, e = 100000, m = 00000000)
		return ((!!s) && (e == exp_t(min_exponent_val)) && !m);
	}
	// SHIFTS
#if VIVADO_OR_CATAPULT == 0
	template<int WI> ct_float operator <<(const ap_int<WI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e + op2;
		r.m = m;
		return r;
	}
	template<int WI> ct_float operator <<(const ap_uint<WI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e + op2;
		r.m = m;
		return r;
	}
#elif VIVADO_OR_CATAPULT == 1
	template<int WI, bool SI> ct_float operator <<(const ac_int<WI,SI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e + op2;
		r.m = m;
		return r;
	}
#endif	

#if VIVADO_OR_CATAPULT == 0	
	template<int WI> ct_float operator >>(const ap_int<WI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e - op2;
		r.m = m;
		return r;
	}
	template<int WI> ct_float operator >>(const ap_uint<WI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e - op2;
		r.m = m;
		return r;
	}
#elif VIVADO_OR_CATAPULT == 1
	template<int WI, bool SI> ct_float operator >>(const ac_int<WI,SI> &op2) const {
		// TODO: add some verifications ?
		ct_float r;
		r.s = s;
		r.e = r.e - op2;
		r.m = m;
		return r;
	}
#endif	

#if VIVADO_OR_CATAPULT == 0	
	template<int WI> ct_float &operator <<=(const ap_int<WI> &op2) {
		*this = operator <<(op2);
		return *this;
	}
	template<int WI> ct_float &operator >>=(const ap_int<WI> &op2) {
		*this = operator >>(op2);
		return *this;
	}
	template<int WI> ct_float &operator <<=(const ap_uint<WI> &op2) {
		*this = operator <<(op2);
		return *this;
	}
	template<int WI> ct_float &operator >>=(const ap_uint<WI> &op2) {
		*this = operator >>(op2);
		return *this;
	}
#elif VIVADO_OR_CATAPULT == 1
	template<int WI, bool SI> ct_float &operator <<=(const ac_int<WI,SI> &op2) {
		*this = operator <<(op2);
		return *this;
	}
	template<int WI, bool SI> ct_float &operator >>=(const ac_int<WI,SI> &op2) {
		*this = operator >>(op2);
		return *this;
	}
#endif	
	// ARITHMETIC
	ct_float operator +() const {
		return *this;
	}
	ct_float operator -() const {
		ct_float r;
		if(operator !()) {
			r = *this;
		} else {
			r.s = !s;
			r.e = e;
			r.m = m;
		}
		return r;
	}
	template<CT_FLOAT_T(2)> ct_float& operator +=(const CT_FLOAT_DEC(2) &op2) {
		this->add_func(op2);
		return *this;
	}
	template<CT_FLOAT_T(2)> ct_float& operator -=(const CT_FLOAT_DEC(2) &op2) {
		CT_FLOAT_DEC(2) op2_temp = op2;
		op2_temp = -op2_temp;
		this->add_func(op2_temp);
		return *this;
	}
	template<CT_FLOAT_T(2)> ct_float& operator *=(const CT_FLOAT_DEC(2) &op2) {
		this->mul_func(op2);
		return *this;
	}
	template<CT_FLOAT_T(2)> typename arith_rt<EXP_W2,MANT_W2>::add_sub_t operator +(const CT_FLOAT_DEC(2) &op2) const {
		typename arith_rt<EXP_W2,MANT_W2>::add_sub_t r = *this;
		r += op2;
		return r;
	}
	template<CT_FLOAT_T(2)> typename arith_rt<EXP_W2,MANT_W2>::add_sub_t operator -(const CT_FLOAT_DEC(2) &op2) const {
		typename arith_rt<EXP_W2,MANT_W2>::add_sub_t r = *this;
		r -= op2;
		return r;
	}
	template<CT_FLOAT_T(2)> typename arith_rt<EXP_W2,MANT_W2>::mul_t operator *(const CT_FLOAT_DEC(2) &op2) const {
		typename arith_rt<EXP_W2,MANT_W2>::mul_t r = *this;
		r *= op2;
		return r;
	}
	// CAST
	operator double() const {
		double r;

#if VIVADO_OR_CATAPULT == 0	
		ap_uint<mantissa_w+1> m_temp = m;
#elif VIVADO_OR_CATAPULT == 1
		ac_int<mantissa_w+1,false> m_temp = m;
#endif	

		if (!(operator !())) {
			m_temp[mantissa_w] = 1; // Adding hidden 1
			r = m_temp.to_double();
			r /= (double) (((long) 1) << ((long) mantissa_w));
			r = r*pow((double) 2.0, (double) (e.to_int()));
			r = s ? -r : r;
		}
		else r = 0.0;
		return r;
	}
	operator float() const {
		// Advised to use only double for more safety.
		double r_temp;
		float r;
#if VIVADO_OR_CATAPULT == 0	
		ap_uint<mantissa_w+1> m_temp = m;
#elif VIVADO_OR_CATAPULT == 1
		ac_int<mantissa_w+1,false> m_temp = m;
#endif	
		if (!(operator !())) {
			m_temp[mantissa_w] = 1; // Adding hidden 1
			r_temp = m_temp.to_double();
			while (r_temp >= 2.0) {
				r_temp /= 2.0;
			}
			r /= (double) (((long) 1) << ((long) mantissa_w));
			r_temp = r_temp*pow((double) 2.0, (double) (e.to_int()));
			r_temp = s ? -r_temp : r_temp;
			r = (float) r_temp;
		}
		else r = 0.0;
		return r;
	}

	// OTHER ATTRIBUTES
	void ct_rand() {
		// srand() should be ran first
		// We perform a rand by slices of 16 bits for the mantissa, and we suppose the exponent is inferior to 16 bits, which is reasonable
		int n_slices_mant = mantissa_w / 16;
		int last_slice_mant_w = mantissa_w % 16;
		int mask_mant = ~((~(int) 0) << 16);// 00....001111111111111111
		int last_mask_mant = ~((~(int) 0) << last_slice_mant_w);
		mant_t ac_rand_temp_mant = 0;
		int rand_temp;
		// random mantissa
		m = 0;
		for(int i = 0; i< n_slices_mant; i++) {
			rand_temp = rand() & mask_mant;
			ac_rand_temp_mant = rand_temp;
			m = m | (ac_rand_temp_mant << (16*i));

		}
		rand_temp = rand() & last_mask_mant;
		ac_rand_temp_mant = rand_temp;
		m = m | (ac_rand_temp_mant << (16*n_slices_mant));

		// random exponent
		rand_temp = min_exponent_val + (rand() % (((int) 1) << exponent_w));
		e = rand_temp;

		s = rand() % 2;
	}

	template<CT_FLOAT_T(2)> void ct_rand_cp(CT_FLOAT_DEC(2) &op2) {
		// modifies *this so it is a random number activating adding close path when added to op2
		// Choosing a random exponent close enough to op2
		//int max_diff_exponent = mantissa_w;
		ct_float op2_temp;
		int n_slices_mant = mantissa_w / 16;
		int last_slice_mant_w = mantissa_w % 16;
		int mask_mant = ~((~(int) 0) << 16);// 00....001111111111111111
		int last_mask_mant = ~((~(int) 0) << last_slice_mant_w);
		mant_t ac_rand_temp_mant = 0;
		int rand_temp;
		op2_temp.operator=(op2);
		// random mantissa
		m = 0;
		for(int i = 0; i< n_slices_mant; i++) {
			rand_temp = rand() & mask_mant;
			ac_rand_temp_mant = rand_temp;
			m = m | (ac_rand_temp_mant << (16*i));

		}
		rand_temp = rand() & last_mask_mant;
		ac_rand_temp_mant = rand_temp;
		m = m | (ac_rand_temp_mant << (16*n_slices_mant));

		// random exponent
		int plusminus_exponent = rand() % 2;
		rand_temp = plusminus_exponent == 0 ? op2.e + (rand() % mantissa_w) : op2.e - (rand() % mantissa_w);
		e = rand_temp;

		s = rand() % 2;

	}

	bool is_in_range(double test_input) {
		// Returns true if "test_input" is in ct_float range
		bool r = false;
		double temp_double;

		double max_val = this->get_max_abs_val_double();
		double min_val = this->get_min_abs_val_double();

		temp_double = test_input >= 0.0 ? test_input : -test_input;
		if( (temp_double >= min_val) && (temp_double <= max_val) ) {
			r = true;
		}
		return r;
	}
};

// TO STRING
template<CT_FLOAT_T()>
std::ostream& operator <<(std::ostream &strm, const CT_FLOAT_DEC()&ctf) {
	double ctf_d;
	ctf_d = (double) ctf;
	return strm << ctf_d;
}

#endif
