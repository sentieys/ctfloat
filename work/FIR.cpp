#include<cstdlib>
#include<ctime>
#include"ctfloat.h"
using namespace std;

#define N_FIR 9
#define N_DATA 50

double h[N_FIR] = { -1.55107884796477e-18, -0.0226639854595526, 1.04697822237622e-17, 0.273977082565524, 0.497373805788057, 0.273977082565524, 1.04697822237622e-17,
    -0.0226639854595526, -1.55107884796477e-18 };

ct_float<8, 24, CT_RD> h_ctf[N_FIR] = { -1.55107884796477e-18, -0.0226639854595526, 1.04697822237622e-17, 0.273977082565524, 0.497373805788057, 0.273977082565524, 1.04697822237622e-17,
    -0.0226639854595526, -1.55107884796477e-18 };


int main(void) {
    ct_float<8, 24, CT_RD> x_ctf[N_DATA], y_ctf[N_DATA - N_FIR];
    double x[N_DATA], y[N_DATA - N_FIR];

	srand(time(NULL));

	// Génération de données aléatoires entre -1 et 1 - non synthétisable
	for (int i = 0; i < N_DATA; i++) {
		x[i] = ((double) rand() / (double) RAND_MAX);
		x[i] = (rand() % 2) ? x[i] : -x[i];
        x_ctf[i] = ct_float<8, 24, CT_RD>(x[i]);
	}

    // Application du filtre - synthétisable
    for (int i = 0; i < N_DATA - N_FIR; i++) {
        y_ctf[i] = x_ctf[i] * h_ctf[N_FIR - 1];
        for (int j = 1; j < N_FIR; j++) {
            y_ctf[i] += x_ctf[i + j] * h_ctf[N_FIR - j - 1];
        }
    }
    
    // Application du filtre de référence
    for (int i = 0; i < N_DATA - N_FIR; i++) {
        y[i] = x[i] * h[N_FIR - 1];
        for (int j = 1; j < N_FIR; j++) {
            y[i] += x[i + j] * h[N_FIR - j - 1];
        }
    }
    
	// Affichage du résultat - non synthétisable
	cout << "FIR d'ordre " << (int) N_FIR - 1 << " (converti en ct_float<" << ct_float<8, 24, CT_RD>::exponent_w << "," << ct_float<8, 24, CT_RD>::mantissa_w + 1 << ">):" << endl;
	for (int i = 0; i < N_FIR; i++) {
		cout << h_ctf[i] << "\t" << h[i] << endl;
	}
	cout << endl;

	cout << "Resultats:" << endl;
	for (int i = 0; i < N_DATA-N_FIR; i++) {
		cout << y_ctf[i] << "\t" << y[i] << endl;
	}

	return EXIT_SUCCESS;
}
