#ifndef _MPFLOAT_H
#define _MPFLOAT_H

#include <mpreal.h>
#include <iostream>

// Shortcut for templated definitions
#define MPFLOAT_T(v) unsigned int EXP_W##v, unsigned int MANT_W##v,  mp_rnd_t R_MODE##v
#define MPFLOAT_W(v) unsigned int EXP_W##v, unsigned int MANT_W##v
#define MPFLOAT_DEC(v) mpfloat<EXP_W##v, MANT_W##v, R_MODE##v >

// comparison functions
#define MP_MAX(a, b) (a) > (b) ? (a) : (b)

template <MPFLOAT_T()>
class mpfloat {

    public:

    // Format specific values
    static const long min_exponent_val = -(((long) 1) << (EXP_W-1)) + 1;
	static const long max_exponent_val = (((long) 1) << (EXP_W-1));
    static const mp_rnd_t rnd_mode_val = R_MODE;
    static const mp_prec_t prec_val = MANT_W;

    // set mpfr parameters in order to construct a 
    // new value of the 'current' type
    static void set_mpreal_hyperparams() {
        mpfr::mpreal::set_default_prec(prec_val);
        mpfr::mpreal::set_emin(min_exponent_val);
        mpfr::mpreal::set_emax(max_exponent_val);
        mpfr::mpreal::set_default_rnd(rnd_mode_val);
    }

    template <MPFLOAT_T(2)> friend class mpfloat;

    // constructors/destructors + assignment operators
    mpfloat() {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = 0;
        v = nv;
    }

    mpfloat(const double in) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = in;
        v = nv;
    }

    mpfloat(const float in) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = in;
        v = nv;
    }

    mpfloat(const mpfr::mpreal& in) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = in.toDouble(R_MODE);
        v = nv;
    }

    template<MPFLOAT_T(2)> mpfloat(const MPFLOAT_DEC(2)& in) {
        set_mpreal_hyperparams();
        // FIXME: this is botch fix because of how the mpreal constructor
        // from another mpreal copies the precision of in into the v
        // variable (hence do not use mpfloat's with precisions larger
        // than float64)
        mpfr::mpreal nv = in.v.toDouble();
        v = nv;
    }

    ~mpfloat() {}

    inline mpfloat& operator =(const double rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = rhs;
        v = nv;
        return *this;
    }

    inline mpfloat& operator =(const float rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = rhs;
        v = nv;
        return *this;
    }

    template<MPFLOAT_T(2)> inline mpfloat& operator =(const MPFLOAT_DEC(2)& rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = rhs.v;
        v = nv;
        return *this;
    }

    // comparison operators
    bool operator ==(const double rhs) {
        return v == rhs;
    }

    bool operator ==(const float rhs) {
        return v == rhs;
    }

    template<MPFLOAT_T(2)> bool operator ==(const MPFLOAT_DEC(2)& rhs) {
        return v == rhs.v;
    }

    bool operator !=(const double rhs) {
        return v != rhs;
    }

    bool operator !=(const float rhs) {
        return v != rhs;
    }

    template<MPFLOAT_T(2)> bool operator !=(const MPFLOAT_DEC(2)& rhs) {
        return v != rhs.v;
    }

    bool operator <=(const double rhs) {
        return v <= rhs;
    }

    bool operator <=(const float rhs) {
        return v <= rhs;
    }

    template<MPFLOAT_T(2)> bool operator <=(const MPFLOAT_DEC(2)& rhs) {
        return v <= rhs.v;
    }

    bool operator <(const double rhs) {
        return v < rhs;
    }

    bool operator <(const float rhs) {
        return v < rhs;
    }

    template<MPFLOAT_T(2)> bool operator <(const MPFLOAT_DEC(2)& rhs) {
        return v < rhs.v;
    }

    bool operator >=(const double rhs) {
        return v >= rhs;
    }

    bool operator >=(const float rhs) {
        return v >= rhs;
    }

    template<MPFLOAT_T(2)> bool operator >=(const MPFLOAT_DEC(2)& rhs) {
        return v >= rhs.v;
    }

    bool operator >(const double rhs) {
        return v > rhs;
    }

    bool operator >(const float rhs) {
        return v > rhs;
    }

    template<MPFLOAT_T(2)> bool operator >(const MPFLOAT_DEC(2)& rhs) {
        return v > rhs.v;
    }

    mpfloat operator +() const {
        return *this;
    }

    mpfloat operator -() const {
        return *this;
    }

    template<MPFLOAT_W(2)> struct arith_rt {
        enum {
            add_sub_exp_w = MP_MAX(EXP_W, EXP_W2),
            add_sub_mant_w = MP_MAX(MANT_W, MANT_W2),
            mul_exp_w = MP_MAX(EXP_W, EXP_W2),
            mul_mant_w = MP_MAX(MANT_W, MANT_W2)
        };
        typedef mpfloat<add_sub_exp_w, add_sub_mant_w, R_MODE> add_sub_t;
        typedef mpfloat<mul_exp_w, mul_mant_w, R_MODE> mul_t;
    };

    template<MPFLOAT_T(2)> mpfloat& operator +=(const MPFLOAT_DEC(2) &rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = this->v + rhs.v;
        this->v = nv;
        return *this;
    }

    template<MPFLOAT_T(2)> mpfloat& operator -=(const MPFLOAT_DEC(2) &rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = this->v - rhs.v;
        this->v = nv;
        return *this;
    }

    template<MPFLOAT_T(2)> mpfloat& operator *=(const MPFLOAT_DEC(2) &rhs) {
        set_mpreal_hyperparams();
        mpfr::mpreal nv = this->v * rhs.v;
        this->v = nv;
        return *this;
    }

    template<MPFLOAT_T(2)> typename arith_rt<EXP_W2, MANT_W2>::add_sub_t operator +(const MPFLOAT_DEC(2) &rhs) const {
        typename arith_rt<EXP_W2, MANT_W2>::add_sub_t r;
        r = *this;
        r += rhs;
        return r;
    }

    template<MPFLOAT_T(2)> typename arith_rt<EXP_W2, MANT_W2>::add_sub_t operator -(const MPFLOAT_DEC(2) &rhs) const {
        typename arith_rt<EXP_W2, MANT_W2>::add_sub_t r;
        r = *this;
        r -= rhs;
        return r;
    }

    template<MPFLOAT_T(2)> typename arith_rt<EXP_W2, MANT_W2>::mul_t operator *(const MPFLOAT_DEC(2) &rhs) const {
        typename arith_rt<EXP_W2, MANT_W2>::mul_t r;
        r = *this;
        r *= rhs;
        return r;
    }

    operator double() const {
        return v.toDouble(R_MODE);
    }

    std::pair<long, long> get_mant_exp() {
        if (v != 0.0) {
            long exp;
            double mant = mpfr_get_d_2exp(&exp, v.mpfr_ptr(), R_MODE);
            long mantl = (long)((((long) 1) << (MANT_W)) * abs(mant)) % (((long) 1) << (MANT_W-1));
            return std::make_pair(mantl, exp-1);
        } else {
            // for consistency with ctfloat
            return std::make_pair(0l, -(((long) 1) << (EXP_W-1)));
        }
    }

    mpfr::mpreal v;

};

template <MPFLOAT_T()>
std::ostream& operator <<(std::ostream &stream, const MPFLOAT_DEC() &rhs) {
    return stream << rhs.v;
}

#endif