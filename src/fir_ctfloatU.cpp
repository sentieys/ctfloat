/* =======================================================================
	Author: Mercier Romain
	file: fir_flp.cpp

	Description: Utilization of ctfloat.h library through a FIR
		     application

======================================================================= */
#include <iostream>
#include "ctfloatU.h"
#include "ac_int.h"
#include "fir_flp.h"

using namespace std;

#define CTFLOAT ct_float<11,53,CT_RD>
#define CTFLOATACC ct_float<11,53,CT_RD>
#define	NDATA	100000
#define NDATA1	NDATA-NUM_TAPS
#define NDATA2	NDATA1-NUM_TAPS
 
int main(void) {

	int k;
	double MSE = 0;
	double input[NDATA], output[NDATA-NUM_TAPS];
	CTFLOAT flp_coeffs[NUM_TAPS], flp_input[NDATA], flp_output[NDATA-NUM_TAPS];
	srand(time(NULL));
	
	// Coefficients obtained by Matlab for a bandpass filter with 33 taps
    double coeffs[NUM_TAPS] = {0.00582912028375713944794433984952775063,0.012559446335867689714671513456778484397,0.01118905222459287696068486184231005609, 0.00498406985516478950176688655915313575,0.014609447913757988776750629256184765836,0.00297226101796868851651067799934935465, 0.02235723882265322828843956415312277386,0.004269405247728785823824182443786412477,0.030245561362284706374836673603567760438, 0.017512757691081633215635093847595271654,0.037881178166983532629163988758591585793,0.04140722224445896743061368283633782994, 0.044219029465996526062632199227664386854,0.091757322729507290182127121624944265932,0.048420094960538409556427552615787135437, 0.313350687080193424094431975390762090683,0.45011118970205699074682570426375605166,0.313350687080193424094431975390762090683, 0.048420094960538409556427552615787135437,0.091757322729507290182127121624944265932,0.044219029465996526062632199227664386854, 0.04140722224445896743061368283633782994,0.037881178166983532629163988758591585793,0.017512757691081633215635093847595271654, 0.030245561362284706374836673603567760438,0.004269405247728785823824182443786412477,0.02235723882265322828843956415312277386, 0.00297226101796868851651067799934935465,0.014609447913757988776750629256184765836,0.00498406985516478950176688655915313575, 0.01118905222459287696068486184231005609,0.012559446335867689714671513456778484397, 0.00582912028375713944794433984952775063};
    
	// Coefficient casting in customized floating-point format
	for(k=0;k<NUM_TAPS;k++) {
		flp_coeffs[k] = coeffs[k];
	}

	// Generation of input data
	gen_pos_input<double,NDATA>(input);

	// Input data casting in customized floating-point format
	for(k=0;k<NDATA;k++) {
		flp_input[k] = input[k];
	}

	// FIR filter computation
	for(k=0;k<NDATA1;k++) {
		fir_filter<double,double>(&input[k],coeffs,&output[k]);
		fir_filter<CTFLOAT,CTFLOATACC>(&flp_input[k],flp_coeffs,&flp_output[k]);
	}

    // Computing the mean square error
    for(k=NUM_TAPS;k<NDATA1;k++) {
		MSE +=  square_err<double,CTFLOAT>(output[k],flp_output[k]);
		
    }
    MSE = MSE/(double)(NDATA2);
	
	// Displaying the mean square error
	cout << "Mean Square Error: " << MSE << endl;
	return 0;
}
