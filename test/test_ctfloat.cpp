//============================================================================
// Name        : test_ctfloat.cpp
// Author      : Olivier Sentieys
// Version     :
// Copyright   : 
// Description : Exhaustive or random tests of the ctfloat library
//============================================================================

//#include <stdio.h>
//#include <stdlib.h>
#include <cstdlib>
#include <cstdio>
#include <vector>
#include <iostream>
#include <limits>
#include <cmath>
#include <ctime>
#include <chrono>

#include "mpreal.h"
#include "mpfloat.h"
#include "ctfloat.h"
using namespace std;

#ifndef EXP
#define EXP 5
#define EXP2 5
#define EXP3 8
#endif
#ifndef MAN
#define MAN 5
#define MAN2 11
#define MAN3 23
#endif

#ifndef VERBOSE 
#define VERBOSE 0
#endif

#define NB_RANDOM_TESTS 500000
#define NB_RANDOM_MAC_TESTS 100

// rounding modes of ctfloat:: CT_RZ, CT_RN
typedef ct_float<EXP, MAN, CT_RN> myfloat;
typedef ct_float<EXP2, MAN2, CT_RN> myfloat2;
typedef ct_float<EXP3, MAN3, CT_RN> myfloat3;

float reldeltaMAX=-INFINITY; 
std::string opMAX;
myfloat aMAX, bMAX, resMAX;
myfloat min_pos, max_pos, min_neg, max_neg, zero;
std::vector<float> errors;
std::vector<float> relerrors;
double ME, MSE, MAE, MSRE, MRAE, MRE;
double MAXAE=-INFINITY; 

typedef mpfloat<EXP, MAN, GMP_RNDN> mympfloat;
typedef mpfloat<EXP2, MAN2, GMP_RNDN> mympfloat2;
typedef mpfloat<EXP3, MAN3, GMP_RNDN> mympfloat3;
//MPFR_RNDN: round to nearest (roundTiesToEven in IEEE 754-2008),
//MPFR_RNDZ: round toward zero (roundTowardZero in IEEE 754-2008),
//MPFR_RNDU: round toward plus infinity (roundTowardPositive in IEEE 754-2008),
//MPFR_RNDD: round toward minus infinity (roundTowardNegative in IEEE 754-2008),
//MPFR_RNDA: round away from zero. 

bool test_wrt_float = true; // if true: test of ctfloat with regards to float
							// if false: test of ctfloat with regards to mpfloat


int testBinaryOp(myfloat a, myfloat b, std::string op){
	//mpfr::mpreal mpA = (double) a;
	//mpfr::mpreal mpB = (double) b;
	//mpfr::mpreal mpRes;
	//double dA = (double) a;
	//double dB = (double) b;
	//double dRes;
	float fa = (float) a;
	float fb = (float) b;
	float fres;
	mympfloat mpa = (mympfloat) a;
	mympfloat mpb = (mympfloat) b;
	mympfloat mpres;
	myfloat res;

	if (op == "+") {
		mpres = mpa + mpb;
		res = a + b;
		fres = fa + fb;
	} else if(op == "-") {
		mpres = mpa - mpb;
		res = a - b;
		fres = fa - fb;
	} else if(op == "*") {
		mpres = mpa * mpb;
		res = a * b;
		fres = fa * fb;
	} else if(op == "+=") {
		mpres = mpa;
		mpres += mpb;
		res = a; 
		res += b;
		fres = fa;
		fres += fb;
	} else if(op == "*=") {
		mpres = mpa;
		mpres *= mpb;
		res = a; 
		res *= b;
		fres = fa;
		fres *= fb;
	/*}else if(op== "/"){
		mpRes=mpA/mpB;
		res = a/b;*/
	} else if(op == "<") {
		mpres = (float) (mpa < mpb);
		res = a < b;
		fres = fa < fb;
	} else if(op == ">") {
		mpres = (float) (mpa > mpb);
		res = a > b;
		fres = fa > fb;
	} else if(op == "<=") {
		mpres = (float) (mpa <= mpb);
		res = a <= b;
		fres = fa <= fb;
	} else if(op == ">=") {
		mpres = (float) (mpa >= mpb);
		res = a >= b;
		fres = fa >= fb;
	} else if(op == "==") {
		mpres = (float) (mpa == mpb);
		res = a == b;
		fres = fa == fb;
	} else {
		std::cout<<"Operator not implemented "<<std::endl;
		return 0;
	}
	
	float mpdelta, delta;
	float relmpdelta, reldelta;
	myfloat resQ, resmpQ;
	float error, relerror;

	// test of ctfloat with regards to mpfloat
	resmpQ = (myfloat) mpres; // mpfloat result quantized to ctfloat
	mpdelta = std::abs((float) resmpQ - (float) res);
	if((float)resmpQ!=0){
		relmpdelta = std::abs(mpdelta / (float) mpres);
	}else{
		relmpdelta = mpdelta;
	}
	// test of ctfloat with regards to float rounded to ctfloat
	resQ = (myfloat) fres; // float result quantized to ctfloat
	delta = std::abs((float) resQ - (float) res);

	if (fres > (float) max_pos) fres = (float) max_pos;
	if (fres < (float) min_neg) fres = (float) min_neg;
	error = fres - (float) res;
	if((float)resQ!=0) {
		reldelta = std::abs(delta / (float) resQ);
		relerror = error / fres;
	} else {
		reldelta = delta;
		relerror = fres;
	}

	errors.push_back(error);
	relerrors.push_back(relerror);

	//std::cout<<"res (ctfloat) = "<<res<<" = "<<a<<op<<b<<"; res (float) = "<<fres<<"; res (float quantized) = "<<resQ<<"; delta = "<<delta<<"; reldelta = "<<reldelta<<"; error = "<<error<<"; relative error = "<<relerror<<std::endl;

	// test of ctfloat with regards to float
	if ((reldelta > 0.0) && test_wrt_float) {
		std::cout<<"res (ctfloat) = "<<res<<" = "<<a<<op<<b<<"; edif:: "<<abs((int) a.get_exponent() - (int) b.get_exponent())<<"; res (float) = "<<fres<<"; res (float quantized) = "<<resQ<<"; delta = "<<delta<<"; reldelta = "<<reldelta<<"; error = "<<error<<"; relative error = "<<relerror<<std::endl;
		//std::cout<<"a:: "<<a.get_sign()<<" "<<a.get_exponent()<<" "<<a.get_mantissa()<<"\tb:: "<<b.get_sign()<<" "<<b.get_exponent()<<" "<<b.get_mantissa()<<"\n";
		if (reldelta>reldeltaMAX) {
			reldeltaMAX = reldelta;
			opMAX = op;
			aMAX = a; bMAX = b; resMAX = res;
		}
		return 1;
	}
	else if ((relmpdelta > 0.0) && !test_wrt_float) {
		// test of ctfloat with regards to mpfloat
		std::cout<<"res (ctfloat) = "<<res<<" = "<<a<<op<<b<<"; res (mpfr) = "<<mpres<<"; res (float) = "<<fres<<"; res (float quantized) = "<<resQ<<"; delta = "<<mpdelta<<"; reldelta = "<<relmpdelta<<std::endl;
		if (relmpdelta>reldeltaMAX) {
			reldeltaMAX = relmpdelta;
			opMAX = op;
			aMAX = a; bMAX = b; resMAX = res;
		}
		return 1;
	}
	else
		return 0;

}

int main(void)
{
	int allpassed;
	bool passed;

    auto t1 = std::chrono::high_resolution_clock::now();

	// simple tests
	myfloat a,b;
	max_pos.sat_max_pos();
	min_pos.sat_min_pos();
	max_neg.sat_max_neg();
	min_neg.sat_min_neg();
	zero.set_to_zero();
	std::cout<<"ctfloat<"<<EXP<<","<<MAN<<">::"<<std::endl;
	std::cout<<"max positive value:: "<<max_pos<<", min positive value:: "<<min_pos<<std::endl;
	std::cout<<"max negative value:: "<<max_neg<<", min negative value:: "<<min_neg<<std::endl;
	std::cout<<"representation of zero:: "<<std::bitset<1>((int) zero.get_sign())<<" "<<bitset<EXP>((int) zero.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) zero.get_mantissa())<<std::endl;
	std::cout<<"quantization mode:: ";
	if (a.get_q_mode() == CT_RZ) std::cout<<"CT_RZ (round to zero)\n";
	else if (a.get_q_mode() == CT_RN) std::cout<<"CT_RN (round to nearest - roundTiesToEven)\n";
	else std::cout<<"Unknown\n";
	std::cout<<" "<<std::endl;

	a= 224;//240;//224;//0.46875;//0.117188;//224;//144;//176;//144;//176;//128;//112;//224;//64;//240;//0.0175781;//208;//0.0585938;//128;//80;//250;//-240;//224;//60;//-176;//-192;//-240;//140;//104;//96;//140;//0.4375;//0.375;//0.21875;//0.00439453;//168;//154;//0.00732422;//0.9375;//0.5;//144.0;//224.0;//0.5;//0.97;
	b= 16;//-0.0044;//-140;//0.00439453125;//0.00390625;//-9;//-7;//-22;//-60;//-60;//22;//;//30;//-64;//-0.0146484;//-208;//-0.0625;//-120;//-36;//-8;//8;//7.5;//176;//-72;//192;//-15;//128;//0.00439453;//0.00537109;//0.00878906;//0.25;//1.96875;//-0.28125;//0.00390625;//0.00390625;//1.0;//0.625;//0.00390625;//3.4;
	std::cout<<a<<": sign= "<<a.get_sign()<<", e= "<<a.get_exponent()<<", m= "<<a.get_mantissa()<<"\n";
	std::cout<<std::bitset<1>((int) a.get_sign())<<" "<<bitset<EXP>((int) a.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) a.get_mantissa())<<std::endl;
	std::cout<<b<<": sign= "<<b.get_sign()<<", e= "<<b.get_exponent()<<", m= "<<b.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) b.get_sign())<<" "<<bitset<EXP>((int) b.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) b.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;

	std::cout<<"ctfloat::"<<std::endl;
	myfloat res;

	res=a+b;
	std::cout<<a<<" + "<<b<<" = "<<res<<": sign= "<<res.get_sign()<<", e= "<<res.get_exponent()<<", m= "<<res.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) res.get_sign())<<" "<<bitset<EXP>((int) res.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) res.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;

	/*res=a-b;
	std::cout<<a<<" - "<<b<<" = "<<res<<": sign= "<<res.get_sign()<<", e= "<<res.get_exponent()<<", m= "<<res.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) res.get_sign())<<" "<<bitset<EXP>((int) res.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) res.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;*/

	/*res=a*b;
	std::cout<<a<<" * "<<b<<" = "<<res<<": sign= "<<res.get_sign()<<", e= "<<res.get_exponent()<<", m= "<<res.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) res.get_sign())<<" "<<bitset<EXP>((int) res.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) res.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;*/

	auto op = " + ";
	mympfloat mpA = (mympfloat) a;
	mympfloat mpB = (mympfloat) b;
	mympfloat mpRes;
	float fRes;

	mpRes = mpA + mpB;
	fRes = ((float) a + (float) b);

	//mpRes = mpA - mpB;
	//fRes = ((float) a - (float) b);

	//mpRes = mpA * mpB; 
	//fRes = ((float) a * (float) b);

	std::cout<<"float::"<<std::endl;
	std::cout<<(float) a<<op<<(float) b<<" = "<<fRes<<"; float quantized = "<<(myfloat) fRes<<"\n\n";

	std::cout<<"mpfloat::"<<std::endl;
	std::cout<<mpA<<op<<mpB<<" = "<<mpRes<<" (float "<<fRes<<")"<<std::endl;
	pair<long, long> decomp = mpRes.get_mant_exp();
	//decomp.first: mantisse
	//decomp.second: exposant
	std::cout<<"e= "<<decomp.second<<", m= "<<decomp.first<<std::endl;
	std::cout<<bitset<EXP>((int) (decomp.second))<<" 1."<<std::bitset<MAN-1>((int) decomp.first)<<std::endl;
	std::cout<<" "<<std::endl;

	//allpassed = testBinaryOp(a,b,"+");
	//allpassed = testBinaryOp(a,b,"-");
	//allpassed = testBinaryOp(a,b,"*");
	//return EXIT_SUCCESS;


	// exhaustive tests
	long nbtest = 0;
	std::vector<std::string> opsb; 
	opsb.push_back("+");
	//opsb.push_back("-");
	opsb.push_back("*");
	//opsb.push_back("/");
	//opsb.push_back("<");
	//opsb.push_back(">");
	//opsb.push_back("<=");
	//opsb.push_back(">=");
	//opsb.push_back("==");
	//opsb.push_back("+=");
	//opsb.push_back("*=");

	mpfr::mpreal::set_default_prec(128);
	mpfr::mpreal::set_default_rnd(MPFR_RNDZ);

	// tests of binary operators

	if (EXP+MAN<=12) {
		// exhaustive tests of binary operators

		std::vector<myfloat> allNbs;

		int total=0;
		for(int s=0;s<2;s++){
		   for(int e=0;e<std::pow(2.0,EXP);e++){
		   	for(int m=0;m<std::pow(2.0,MAN-1);m++){
				total++;
				a.set_sign(s);
				a.set_exponent(e);
				a.set_mantissa(m);
				allNbs.push_back(a);
				//std::cout<<a<<" m= "<<a.get_mantissa()<<" e= "<<a.get_exponent()<<" sign= "<<a.get_sign()<<std::endl;
			}
		   }
		}
		//std::cout<<" total tested = "<<total<<" :: "<<std::pow(2.0,MAN+EXP)<<std::endl;
		std::sort(allNbs.begin(), allNbs.end());
		/*for(int i=0;i<allNbs.size();i++){
			std::cout<<allNbs[i]<<" sign= "<<allNbs[i].get_sign()<<" e= "<<allNbs[i].get_exponent()<<" m= "<<allNbs[i].get_mantissa()<<std::endl;
			std::cout<<allNbs[i]<<": "<<std::bitset<1>((int) allNbs[i].get_sign())<<" "<<std::bitset<EXP>((int) allNbs[i].get_exponent())<<" 1."<<std::bitset<MAN-1>((int) allNbs[i].get_mantissa())<<std::endl;
		}*/

		allpassed = 0;
		for(int o=0;o<opsb.size();o++){
			std::cout<<"Testing operator:: "<<opsb[o]<<"\t.....\n";
			for(int i=0;i<allNbs.size();i++){
				//for(int j=i;j<allNbs.size();j++){
				for(int j=0;j<allNbs.size();j++){
					if ((opsb[o] == "-") && (allNbs[j] == min_pos)) {
						// Do not test for subtraction with min_pos
					}
					/*else if ((abs((int) allNbs[i].get_exponent() - (int) allNbs[j].get_exponent()) >= 2*MAN+2) && (allNbs[i].get_q_mode() == CT_RZ)) {
						// Do not test for large exponent difference and with CT_RZ rounding mode
					}*/
					else if ((abs((int) allNbs[i].get_exponent() - (int) allNbs[j].get_exponent()) <= 1) && (allNbs[i].get_sign() != allNbs[j].get_sign())) {
						// Test of close path only
						passed = testBinaryOp(allNbs[i],allNbs[j],opsb[o]);
						nbtest++;
						allpassed += passed;					
					}
					else if (allNbs[i].get_sign() == allNbs[j].get_sign()) {
						// Test of ADDITION in far path 
						passed = testBinaryOp(allNbs[i],allNbs[j],opsb[o]);
						nbtest++;
						allpassed += passed;			

					}
					else {
						// Test of rest of far path 
						passed = testBinaryOp(allNbs[i],allNbs[j],opsb[o]);
						//if (!passed) std::cout<<"Not passed:: "<<opsb[o]<<"\t i:: "<<i<<"\t j:: "<<j<<"\t allNbs[i]:: "<<allNbs[i]<<"\t allNbs[j]:: "<<allNbs[j]<<std::endl;
						nbtest++;
						allpassed += passed;			
					}
				}
			}
		}
	}
	else {
		// NB_RANDOM_TESTS random tests of binary operators
		allpassed = 0;
		srand(time(NULL)); // srand() must be executed before using ct_rand()
		myfloat rnd1, rnd2;
		for(int o=0;o<opsb.size();o++) {
			std::cout<<"Testing operator:: "<<opsb[o]<<"\t.....\n";
			for(int i=0;i<NB_RANDOM_TESTS;i++){
				rnd1.ct_rand();
				rnd2.ct_rand();
				passed = testBinaryOp(rnd1,rnd2,opsb[o]);
				nbtest++;
				allpassed += passed;	
				/*std::cout<<rnd1<<": sign= "<<rnd1.get_sign()<<" e= "<<rnd1.get_exponent()<<" m= "<<rnd1.get_mantissa();
				std::cout<<" "<<std::bitset<1>((int) rnd1.get_sign())<<" "<<std::bitset<EXP>((int) rnd1.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) rnd1.get_mantissa())<<std::endl;
				std::cout<<rnd2<<": sign= "<<rnd2.get_sign()<<" e= "<<rnd2.get_exponent()<<" m= "<<rnd2.get_mantissa();
				std::cout<<" "<<std::bitset<1>((int) rnd2.get_sign())<<" "<<std::bitset<EXP>((int) rnd2.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) rnd2.get_mantissa())<<std::endl;
				std::cout<<std::endl;*/

			}	
		}
	}

	std::cout<<"\n"<<nbtest<<" tests\n";
	if (allpassed==0) 
		std::cout<<"All tests passed! "<<std::endl;
	else
		std::cout<<allpassed<<" errors\nMAX:reldelta :: res = "<<resMAX<<" = "<<aMAX<<opMAX<<bMAX<<" reldelta = "<<reldeltaMAX<<std::endl;

	ME = 0; MSE = 0; MAE = 0; MSRE = 0; MRAE = 0; MRE = 0;
	for(int i=0;i<errors.size();i++){
		MSE +=  (double) errors[i] * (double) errors[i];
		MAE +=  abs((double) errors[i]);
		ME +=  (double) errors[i];
		MSRE +=  (double) relerrors[i] * (double) relerrors[i];
		MRE +=  (double) relerrors[i];
		MRAE +=  abs((double) relerrors[i]);
		if (abs((double) errors[i])>MAXAE) {
			MAXAE = abs((double) errors[i]);
		}	
	}

    MSE = MSE / ((double) errors.size());
    MAE = MAE / ((double) errors.size());
    ME = ME / ((double) errors.size());
    MSRE = MSRE / ((double) errors.size());
    MRE = MRE / ((double) errors.size());
    MRAE = MRAE / ((double) errors.size());

	// Displaying the mean square error
	cout << "\nMean Error: " << ME << endl;
	cout << "Mean Square Error: " << MSE << endl;
	cout << "Mean Absolute Error: " << MAE << endl;
	cout << "Maximum Absolute Error: " << MAXAE << endl;
	cout << "Mean Relative Error: " << MRE << endl;
	cout << "Mean Square Relative Error: " << MSRE << endl;
	cout << "Mean Absolute Relative Error: " << MRAE << endl;

    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::seconds>(t2 - t1).count();
    std::cout << "Elapsed time: " << duration << " sec.\n\n\n";


	//return EXIT_SUCCESS;

	// simple tests of double-precision MAC

	myfloat2 a2,b2,res2;
	myfloat3 a3,b3,res3;

	a = 0.77;
	b = 3.4;
	std::cout<<"ctfloat<"<<EXP<<","<<MAN<<">::"<<std::endl;
	std::cout<<a<<": sign= "<<a.get_sign()<<", e= "<<a.get_exponent()<<", m= "<<a.get_mantissa()<<"\n";
	std::cout<<std::bitset<1>((int) a.get_sign())<<" "<<bitset<EXP>((int) a.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) a.get_mantissa())<<std::endl;
	std::cout<<b<<": sign= "<<b.get_sign()<<", e= "<<b.get_exponent()<<", m= "<<b.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) b.get_sign())<<" "<<bitset<EXP>((int) b.get_exponent())<<" 1."<<std::bitset<MAN-1>((int) b.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;

	std::cout<<"Test of conversion to other ctfloat formats::"<<std::endl;
	a2 = 0.77;
	b2 = 3.4;
	a3 = 0.77;
	b3 = 3.4;

	std::cout<<"ctfloat<"<<EXP2<<","<<MAN2<<">::"<<std::endl;
	std::cout<<a2<<": sign= "<<a2.get_sign()<<", e= "<<a2.get_exponent()<<", m= "<<a2.get_mantissa()<<"\n";
	std::cout<<std::bitset<1>((int) a2.get_sign())<<" "<<bitset<EXP2>((int) a2.get_exponent())<<" 1."<<std::bitset<MAN2-1>((int) a2.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;

	std::cout<<"ctfloat<"<<EXP3<<","<<MAN3<<">::"<<std::endl;
	std::cout<<b3<<": sign= "<<b3.get_sign()<<", e= "<<b3.get_exponent()<<", m= "<<b3.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) b3.get_sign())<<" "<<bitset<EXP3>((int) b3.get_exponent())<<" 1."<<std::bitset<MAN3-1>((int) b3.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;


	std::cout<<"Test of double-precision MAC::"<<std::endl;
	res2=a2*b2;
	std::cout<<a2<<" * "<<b2<<" = "<<res2<<": sign= "<<res2.get_sign()<<", e= "<<res2.get_exponent()<<", m= "<<res2.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) res2.get_sign())<<" "<<bitset<EXP2>((int) res2.get_exponent())<<" 1."<<std::bitset<MAN2-1>((int) res2.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;

	mympfloat2 mpA2 = (mympfloat2) a2;
	mympfloat2 mpB2 = (mympfloat2) b2;
	mympfloat2 mpRes2 = mpA2 * mpB2; 
	fRes = (float) a2 * (float) b2;
	std::cout<<"mpfloat:: ";
	std::cout<<mpA2<<" * "<<mpB2<<" = "<<mpRes2<<" (float "<<fRes<<")\n\n";

	res3 = b3;
	res3 += (myfloat3) res2;
	std::cout<<res2<<" + "<<b3<<" = "<<res3<<": sign= "<<res3.get_sign()<<", e= "<<res3.get_exponent()<<", m= "<<res3.get_mantissa()<<std::endl;
	std::cout<<std::bitset<1>((int) res3.get_sign())<<" "<<bitset<EXP3>((int) res3.get_exponent())<<" 1."<<std::bitset<MAN3-1>((int) res3.get_mantissa())<<std::endl;
	std::cout<<" "<<std::endl;


	// NB_RANDOM_MAC_TESTS random tests of MAC
	allpassed = 0;
	srand(time(NULL)); // srand() must be executed before using ct_rand()
	#define	NUM_TAPS 33
	double fcoeffs[NUM_TAPS] = {-0.00582912028375713944794433984952775063,0.012559446335867689714671513456778484397,0.01118905222459287696068486184231005609, -0.00498406985516478950176688655915313575,-0.014609447913757988776750629256184765836,0.00297226101796868851651067799934935465, 0.02235723882265322828843956415312277386,0.004269405247728785823824182443786412477,-0.030245561362284706374836673603567760438, -0.017512757691081633215635093847595271654,0.037881178166983532629163988758591585793,0.04140722224445896743061368283633782994, -0.044219029465996526062632199227664386854,-0.091757322729507290182127121624944265932,0.048420094960538409556427552615787135437, 0.313350687080193424094431975390762090683,0.45011118970205699074682570426375605166,0.313350687080193424094431975390762090683, 0.048420094960538409556427552615787135437,-0.091757322729507290182127121624944265932,-0.044219029465996526062632199227664386854, 0.04140722224445896743061368283633782994,0.037881178166983532629163988758591585793,-0.017512757691081633215635093847595271654, -0.030245561362284706374836673603567760438,0.004269405247728785823824182443786412477,0.02235723882265322828843956415312277386, 0.00297226101796868851651067799934935465,-0.014609447913757988776750629256184765836,-0.00498406985516478950176688655915313575, 0.01118905222459287696068486184231005609,0.012559446335867689714671513456778484397, -0.00582912028375713944794433984952775063};
	double finput, foutput;
	myfloat coeffs[NUM_TAPS], input, output, regs[NUM_TAPS], foutputQ;
	for(int k=0;k<NUM_TAPS;k++) {
		coeffs[k] = (myfloat) fcoeffs[k];
		regs[k] = 0.0;
	}

	for(int j=0; j<NB_RANDOM_MAC_TESTS; j++){

		//input.ct_rand();
		if (j<50) input = 1.0;
		else input = 0.0;
		cout << "input:: " << input << "\n";
		myfloat mult;
		myfloat2 temp = 0.0;
		double ftemp = 0.0;
		double fmult;

		for (int i = NUM_TAPS-1; i>=0; i--) {
			if (i == 0) 
				regs[i] = input;
			else 
				regs[i] = regs[i-1];
		}

		for (int i = NUM_TAPS-1; i>=0; i--) {
			// ctfloat filter
			mult = coeffs[i] * regs[i];
			temp = temp + (myfloat2) mult;
			cout<<"ctfloat:: "<<coeffs[i]<<" * "<<regs[i]<<" = "<<mult<<" ("<<(myfloat2) mult<<"); temp = "<<temp<<"\n";
			// double-precision filter
			fmult = (double) coeffs[i] * (double) regs[i];
			ftemp = ftemp + fmult;
			cout<<"float:: "<<coeffs[i]<<" * "<<regs[i]<<" = "<<fmult<<" ("<<(myfloat2) fmult<<"); temp = "<<ftemp<<"\n";
		}
		output = (myfloat) temp;
		foutput = ftemp;
		foutputQ = (myfloat) foutput;
		cout << "output:: " << output << " (ctfloat); " << foutput << " (double); " << foutputQ << " (double quantized)\n";




		nbtest++;

	}



	return EXIT_SUCCESS;
}




