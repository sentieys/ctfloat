#include "mpfloat.h"
#include <cmath>
#include <iostream>
#include <limits>
#include <cstdlib>
#include <cstdio>
#include <vector>

#ifndef EXP
#define EXP 4
#endif
#ifndef MAN
#define MAN 4
#endif

typedef mpfloat<EXP, MAN, GMP_RNDZ> mympfloat;
//MPFR_RNDN: round to nearest (roundTiesToEven in IEEE 754-2008),
//MPFR_RNDZ: round toward zero (roundTowardZero in IEEE 754-2008),
//MPFR_RNDU: round toward plus infinity (roundTowardPositive in IEEE 754-2008),
//MPFR_RNDD: round toward minus infinity (roundTowardNegative in IEEE 754-2008),
//MPFR_RNDA: round away from zero. 

float relDeltaMAX=-INFINITY;
std::string opMAX;
mympfloat aMAX, bMAX, resMAX;

bool testBinaryOp(mympfloat a, mympfloat b, std::string op){
 
    float fA = (float) a;
    float fB = (float) b;
    float fRes;
    mympfloat res, resQ;
    float delta, relDelta;

    if(op== "+"){
        res = a+b;
        fRes = fA+fB;
    }else if(op== "-"){
        res = a-b;
        fRes = fA-fB;
    }else if(op== "*"){
        res = a*b;
        fRes = fA*fB;
    /*}else if(op== "/"){
        mpRes=mpA/mpB;
        res = a/b;*/

    }
 
    // test of mpfloat with regards to float
    resQ = (mympfloat) fRes; // float result quantized to mympfloat
    delta = std::abs((float) resQ - (float) res);
    if((float)resQ!=0){
        relDelta = std::abs(delta / (float) resQ);
    }else{
       relDelta = delta;
    }       

    if (relDelta > 0.0) {
        // test of ctfloat with regards to float
        std::cout<<"res (mpfloat) = "<<res<<" = "<<a<<op<<b<<"; res (float) = "<<fRes<<"; res (float quantized) = "<<resQ<<"; delta = "<<delta<<"; reldelta = "<<relDelta<<std::endl;
        if (relDelta>relDeltaMAX) {
            relDeltaMAX = relDelta;
            opMAX = op;
            aMAX = a; bMAX = b; resMAX = res;
        }
        return false;
    }
    else
        return true;

}

int main() {

    mpfloat<11, 53, GMP_RNDN> v1 = M_PI;
    double dv1 = M_PI;

    mpfloat<11, 53, GMP_RNDN> v2 = std::numeric_limits<double>::min();
    double dv2 = std::numeric_limits<double>::min();

    mpfloat<11, 53, GMP_RNDN> v3 = 1.41f;
    double dv3 = (double)1.41f;

    mpfloat<8, 24, GMP_RNDN> v4 = M_PI;
    float fv4 = M_PI;

    mpfloat<8, 24, GMP_RNDN> v5 = v2;
    float fv5 = dv2;

    mpfloat<11, 53, GMP_RNDN> op1 = std::exp(1.2);
    double dop1 = std::exp(1.2);

    mpfloat<8, 24, GMP_RNDN> op2 = std::log10(.67f);
    float fop2 = std::log10(.67f);

    mpfloat<8, 24, GMP_RNDN> res11 = op1 + op2;
    mpfloat<11, 53, GMP_RNDN> res12 = op1 + op2;
    float fres11 = dop1 + fop2;
    double dres12 = dop1 + fop2;

    mpfloat<8, 24, GMP_RNDN> res21 = op2 - op1;
    mpfloat<11, 53, GMP_RNDN> res22 = op2 - op1;
    float fres21 = fop2 - dop1;
    double dres22 = fop2 - dop1;

    mpfloat<8, 24, GMP_RNDN> res31 = op2 * op1;
    mpfloat<11, 53, GMP_RNDN> res32 = op2 * op1;
    float fres31 = fop2 * dop1;
    double dres32 = fop2 * dop1;

    mpfloat<4, 4, GMP_RNDN> op3 = 0.5;
    mpfloat<4, 4, GMP_RNDN> op4 = 0.00390625;    
    mpfloat<4, 4, GMP_RNDN> res33 = op3 * op4;
    float fres33 = (float) op3 * (float) op4;

    double convTest = res32;

    mpfloat<8, 24, GMP_RNDN> res4 = 0.5;
    auto decomp = res4.get_mant_exp();

    typedef mpfloat<4, 4, GMP_RNDN> mympfloat;

    std::cout << "Starting suite of tests...\n";
    // this should be true for all examples
    if (v1 == dv1)
        std::cout << "Test 1 PASSED!\n";
    else
        std::cout << "Test 1 FAILED!\n";
    if (v2 == dv2)
        std::cout << "Test 2 PASSED!\n";
    else
        std::cout << "Test 2 FAILED!\n";
    if (v3 == dv3)
        std::cout << "Test 3 PASSED!\n";
    else
        std::cout << "Test 3 FAILED!\n";
    if (v4 == fv4)
        std::cout << "Test 4 PASSED!\n";
    else
        std::cout << "Test 4 FAILED!\n";
    if (v5 == fv5)
        std::cout << "Test 5 PASSED!\n";
    else
        std::cout << "Test 5 FAILED!\n";
    if (res11 == fres11)
        std::cout << "Test 6 PASSED!\n";
    else
        std::cout << "Test 6 FAILED!\n";
    if (res12 == dres12)
        std::cout << "Test 7 PASSED!\n";
    else
        std::cout << "Test 7 FAILED!\n";
    if (res21 == fres21)
        std::cout << "Test 8 PASSED!\n";
    else
        std::cout << "Test 8 FAILED!\n";
    if (res22 == dres22)
        std::cout << "Test 9 PASSED!\n";
    else
        std::cout << "Test 9 FAILED!\n";
    if (res31 == fres31)
        std::cout << "Test 10 PASSED!\n";
    else
        std::cout << "Test 10 FAILED!\n";
    if (res32 == dres32)
        std::cout << "Test 11 PASSED!\n";
    else
        std::cout << "Test 11 FAILED!\n";
    if (decomp.first == 0l and decomp.second == -1l)
        std::cout << "Test 12 PASSED!\n";
    else
        std::cout << "Test 12 FAILED!\n";
    /*if (res33 == 0.00390625)
        std::cout << "Test 13 PASSED!\n";
    else {
        std::cout << "Test 13 FAILED! "<<"float: "<<fres33<<" mpfloat: "<<res33<<"\n";
    }*/

    std::cout << "\nTesting all possible values of mpfloat<"<<EXP<<", "<<MAN<<">\n";


    std::vector<std::string> opsb; 
    opsb.push_back("+");
    opsb.push_back("-");
    opsb.push_back("*");
    //opsb.push_back("/");

    std::vector<std::string> opsu;
    opsu.push_back("+=");
    opsu.push_back("*=");
    opsu.push_back("/=");

    std::vector<mympfloat> allNbs;

    // FIXME only for <4,4>, need to be generalized
    mympfloat mprev = -256.0;
    allNbs.push_back(mprev);
    mympfloat mnext = mpfr::nextabove(mprev.v);
    decomp = mprev.get_mant_exp();
    //std::cout << mprev << " sign = " << (mprev <= mympfloat(0.0)) << " e = " << decomp.second << " m = " << decomp.first << std::endl;
    while(mprev != mnext) {
        mprev = mnext;
        allNbs.push_back(mprev);
        decomp = mprev.get_mant_exp();
        //std::cout << mprev << " sign = " << (mprev <= mympfloat(0.0)) << " e = " << decomp.second << " m = " << decomp.first << std::endl;
        mnext = mpfr::nextabove(mprev.v);
    }

    // exhaustive tests of binary operators
    bool allpassed=true;
    for(int o=0;o<opsb.size();o++){
        for(int i=0;i<allNbs.size();i++){
            for(int j=0;j<allNbs.size();j++){
                bool passed=testBinaryOp(allNbs[i],allNbs[j],opsb[o]);
                allpassed = allpassed && passed;
            }
        }
    }

    if (allpassed) 
        std::cout<<"\nAll tests passed! "<<std::endl;
    else
        std::cout<<"\nMAX:reldelta :: res = "<<resMAX<<" = "<<aMAX<<opMAX<<bMAX<<" reldelta = "<<relDeltaMAX<<std::endl;




    std::cout << "Finished running all tests!\n";
}
