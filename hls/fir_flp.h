/* =======================================================================
	Authors: Mercier Romain, Olivier Sentieys
    Date: 22 June 2018
	File: fir_flp.h

 fir_filter: Templated FIR function
 gen_coeffs, gen_input: Generation of random coefficients and input data
 square_err: Computes square error between two values
======================================================================= */

#ifndef _FIR_FILTER_H
#define _FIR_FILTER_H

#define		NUM_TAPS	33

// Templated FIR function
template<typename T, typename ACC> void fir_filter(T *input, T coeffs[NUM_TAPS], T* output ) {
	static T regs[NUM_TAPS];
	ACC temp = 0.0;
	int i; 

	for ( i = NUM_TAPS-1; i>=0; i--) {
		if ( i == 0 ) 
			regs[i] = *input;
		else 
			regs[i] = regs[i-1];
	}

	for ( i = NUM_TAPS-1; i>=0; i--) {
		temp += coeffs[i]*regs[i];
	}

	*output = temp;
}

// Generation of normalized coefficients between -1 and 1
template<typename T, int N> void gen_coeffs(T* coeffs) {
	int i;
	for(i=0;i<N;i++) {
		coeffs[i] = (T)(100*((double)rand())/((double)(RAND_MAX))-1);
	}
}

// Generation of input data normalized between -1 and 1
template<typename T, int N> void gen_input(T* input) {
	int i;
	for(i=0;i<N;i++) {
		input[i] = (T)(2*((double)rand())/((double)(RAND_MAX))-1);
	}
}

// Generation of input data normalized between 0 and 2
template<typename T, int N> void gen_pos_input(T* input) {
	int i;
	for(i=0;i<N;i++) {
		input[i] = (T)(2*((double)rand())/((double)(RAND_MAX)));
	}
}

// Computes square error between two values
template<typename T1,typename T2> T1 square_err(T1 reference, T2 approximation) {
	T1 SE = 0;

	SE = (reference - (T1)(approximation))*(reference - (T1)(approximation));
	
	return SE;
}

#endif
